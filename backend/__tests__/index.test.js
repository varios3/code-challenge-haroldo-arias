const request = require('supertest')
const Servidor = require('../src/servidor')
const config = require('../src/utils/config')
const expect = require('chai').expect

const app = new Servidor.Servidor(config.PORT_TEST)
app.listen()

describe('UNIT TEST FOR API', function () {
  describe('TEST FOR iecho ENDPOINT (GET)', async function () {
    it('VALID DATA FOR ENV', async function () {
      expect(config.PORT).to.eql('3000')
      expect(config.PORT_TEST).to.eql('9000')
      expect(config.RUTA).to.eql('iecho')
    })

    it('VALID RESPONSE WITH NO PALINDROME', async function () {
      const response = await request(app.app).get('/iecho?text=hola')
      expect(response.status).to.eql(200)
      expect(response.body.palindrome).to.eql(false)
      expect(response.body.text).to.eql('aloh')
    })

    it('VALID RESPONSE WITH PALINDROME', async function () {
      const response = await request(app.app).get('/iecho?text=bob')
      expect(response.status).to.eql(200)
      expect(response.body.palindrome).to.eql(true)
      expect(response.body.text).to.eq('bob')
    })

    it('VALID RESPONSE WITH NOT PARAMETER', async function () {
      const response = await request(app.app).get('/iecho')
      expect(response.status).to.eql(400)
      expect(response.body.error).to.eq('no text')
    })

    it('VALID RESPONSE WITH EMPTY PARAMETER', async function () {
      const response = await request(app.app).get('/iecho?').query({ text: '' })
      expect(response.status).to.eql(400)
      expect(response.body.error).to.eq('no text')
    })
  })
})
