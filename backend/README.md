# Backend Code Challenge

A continuación se detallan los comandos y dependencias para correr el api

## Comandos

### `npm dev`

Desplegará el api usando nodemon en el puerto 3000 o el seteado en las variables de entorno.\
Utilizar [http://localhost:3000](http://localhost:3000) para realizar las peticiones al API.

El API se recargará automaticamente al aplicar cualquier cambio.\

### `npm test`

Lanzará todas las pruebas unitarias desarrolladas en la aplicación utilizando Mocha y Chai.

### `npm test`

Lanzará todas las pruebas unitarias desarrolladas en la aplicación utilizando Mocha, Chai y SuperTest.

### `npm coverage`

Lanzará todas las pruebas unitarias desarrolladas en la aplicación utilizando Mocha, Chai y SuperTest y adicional generará un detalle del coverage de la aplicación en formato HTML.

### `npm run start`

Lanzará la versión de producción del API


## Depenencias

### `express`

Como servidor principal del servicio.

### `cors`

Para manipular las cabeceras HTML y permitir fuentes no seguras.

### `dotenv`

Para manipular las variables de entorno.

### `chai y mocha`

Para definir el set de pruebas del API

### `supertest`

Para generar un servidor temporal del API en tiempo de ejecución de las pruebas

### `nodemon`

Como servidor en desarrollo para facilitar la recarga de archivos en el servidor