'use strict'
const express = require('express')
const router = express.Router()
const index = require('../controllers/index.controller')

// definición de rutas
router.route('/').get(index.getItem)

exports.default = router
