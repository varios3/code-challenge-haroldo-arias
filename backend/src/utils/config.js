'use strict'
const dotenv = require('dotenv')
const path = require('path')

dotenv.config()
const ruta = path.join(__dirname, '/../../.env.desarrollo')

// si existen mas entornos descomentar
/*
switch (process.env.NODE_ENV) {
    case "qa":
        ruta = `${__dirname}/../../.env.qa`;
        break;
    case "master":
        ruta = `${__dirname}/../../.env.produccion`;
        break;
    default:
        ruta = `${__dirname}/../../.env.desarrollo`;
} */

dotenv.config({ path: ruta })
exports.PORT = process.env.PORT
exports.PORT_TEST = process.env.PORT_TEST
exports.RUTA = process.env.RUTE_ENV
