'use strict'

const express = require('express')
const morgan = require('morgan')
const indexRoutes = require('./routes/index.routes')
const cors = require('cors')
const http = require('http')
const config = require('./utils/config')

class Servidor {
  constructor (port) {
    this.port = port

    // options for cors midddleware
    this.options = {
      allowedHeaders: [
        'Origin',
        'X-Requested-With',
        'Content-Type',
        'Accept',
        'X-Access-Token'
      ],
      credentials: true,
      methods: 'GET,HEAD,OPTIONS,PUT,PATCH,POST,DELETE',
      origin: '*',
      preflightContinue: false
    }

    this.app = express()
    this.app.use(cors(this.options))
    this.settings()
    this.middlewares()
    this.routes()
    this.server = new http.Server()
  }

  settings () {
    this.app.set('port', this.port)
  }

  middlewares () {
    this.app.use(morgan('dev'))
    this.app.use(express.json())
  }

  routes () {
    this.app.use(`/${config.RUTA}`, indexRoutes.default)
  }

  listen () {
    this.server = this.app.listen(this.app.get('port'))
    console.log(
      'Server on port ',
      ':' + this.app.get('port') + '/' + config.RUTA
    )
    return this.server
  }
}
exports.Servidor = Servidor
