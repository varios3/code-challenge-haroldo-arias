'use strict'

const Item = require('../models/Item')

function getItem (req, res) {
  const excepcion = {
    error: ''
  }

  if (req.query.text !== undefined && req.query.text !== '') {
    try {
      const item = new Item.Item(req.query.text.toString())
      const response = item.resolver()
      return res
        .status(200)
        .json(response)
    } catch (error) {
      excepcion.error = error
    }
  } else {
    excepcion.error = 'no text'
  }

  return res
    .status(400)
    .json(excepcion)
}
exports.getItem = getItem
