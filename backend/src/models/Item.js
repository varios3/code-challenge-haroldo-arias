'use strict'

class Item {
  constructor (data) {
    this.data = data
  }

  resolver () {
    const response = {
      text: this.invetir(),
      palindrome: this.isPalindromo()
    }

    return response
  }

  invetir () {
    return this.data.split('').reverse().join('')
  }

  isPalindromo () {
    return this.data === this.invetir()
  }
}

exports.Item = Item
