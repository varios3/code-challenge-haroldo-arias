const conf = require('./src/utils/config')
const Servidor = require('./src/servidor')

function main () {
  const app = new Servidor.Servidor(conf.PORT || 3000)
  app.listen()
}

main()
