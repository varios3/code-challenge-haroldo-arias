# Code Challenge Haroldo Arias

La aplicación puede ejecutarse por 2 vías:

## docker-compose

Solo basta con correr el siguiente comando: 

    docker-compose up -d --build

Esto expondra nuestro servicio web en el puerto 8080 de nuestro explorador

Abrir [http://localhost:8080](http://localhost:8080) para visualizar en el explorador.


## npm

Para correr tanto el frontend y backend es necesario ingresar por 2 consolas diferentes de la aplicación y correr lo siguientes comandos:

### Backend
    
        npm run start

### FronEnd

        npm run dev

Esto expondra nuestro servicio web en el puerto 8080 de nuestro explorador

Abrir [http://localhost:8080](http://localhost:8080) para visualizar en el explorador.