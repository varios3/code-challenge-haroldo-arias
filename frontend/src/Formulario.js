import 'bootstrap/dist/css/bootstrap.min.css';
import { 
  Button, 
  Card, 
  Col, 
  Row,
  Form, 
  FormGroup, 
  Container, 
  InputGroup, 
  FormControl
} from 'react-bootstrap';
import axios from 'axios';
import React, { useState } from 'react';

function Formulario() {
  const[txtIngreso,setTextInput] = useState("");
  const[txtInput1,setTextInput1] = useState("");
  const[txtInput2,setTextInput2] = useState("");
  const[txtInput3,setTextInput3] = useState("");
  const serviceUrl = process.env.RUTA;

  const onClick  = ()=>{
    axios.get(`${serviceUrl}${txtIngreso}`)
    .then(response =>{
      setTextInput1(`${response.data.text}`);
    }).catch(err => {
      alert(`Error en servicio!! ${err}`);
      console.log(err);
    })
    
    setTextInput2(`${txtInput1}`);
    setTextInput3(`${txtInput2}`);
  
  }

  return (
    <Container style={{maxWidth: "100%"}}>
      <Container style={{maxWidth: "100%"}}>
        <Card className="bg-danger">
          <Form style ={{marginTop: "10px"}}>
            <FormGroup>
              <Row >
                <Col sm={2}>
                  &nbsp;
                </Col>
                <Col sm={8}>
                  <InputGroup className="mb-3">
                    <FormControl
                      placeholder="Insert Text"
                      onChange ={(e)=> setTextInput(e.target.value)}
                      id="buscador"
                      itemID='buscador'
                    />
                  </InputGroup>
                </Col>
                <Col sm={2}>
                  <Button onClick={onClick} variant="primary" id="boton"
                      itemID='boton'>
                      Send!
                  </Button>
                </Col>
              </Row>
            </FormGroup>
          </Form>
        </Card>
      </Container>
      <Container style={{maxWidth: "80%", maxHeight:"100%"}}>
        <Card style ={{marginTop: "3%", height:"400px", border:0}}>
            <Row>
                <Col sm={1}>
                  &nbsp;
                </Col>
                <Col sm={10}>
                  <h1>Results:</h1>
                </Col>
            </Row>
            <FormGroup style ={{marginTop: "1%"}}>
              <Row>
                <Col sm={2}>
                  &nbsp;
                </Col>
                <Col sm={8}>
                  <InputGroup className="mb-3">
                    <FormControl
                      placeholder="Third Text"
                      value ={txtInput1}
                      readOnly
                    />
                  </InputGroup>
                  <InputGroup className="mb-3">
                    <FormControl
                      placeholder="Secord Text"
                      value = {txtInput2}
                      readOnly
                    />
                  </InputGroup>
                  <InputGroup className="mb-3">
                    <FormControl
                      placeholder="First Text"
                      value ={txtInput3}
                      readOnly
                    />
                  </InputGroup>
                </Col>
                <Col sm={2}>
                  &nbsp;
                </Col>
              </Row>
            </FormGroup>
          </Card>    
      </Container>
    </Container>

  );
}

export default Formulario;
