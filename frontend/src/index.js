import React from 'react';
import ReactDOM from 'react-dom';
import Formulario from './Formulario';
import './index.css';
import reportWebVitals from './reportWebVitals';
import configureStore from './configureStore'

//const store = configureStore()

ReactDOM.render(
   //<Provider store={store}>
    <Formulario/>,
  //</Provider>,
  document.getElementById('root')
);

reportWebVitals();