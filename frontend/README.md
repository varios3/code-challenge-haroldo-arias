# FrontEnd Code Challenge

A continuación se detallan los comandos y dependencias para correr la aplicación

## Comandos

### `npm run dev`

Utiliza webpack para generar un servidor de la aplicación en modo desarrollo en el puerto 8080.\
Abrir [http://localhost:8080](http://localhost:8080) para visualizar en el explorador.

La página se recargará automaticamente al aplicar cualquier cambio.\

### `npm test`

Lanzará todas las pruebas unitarias desarrolladas en la aplicación utilizando Jest.

### `npm run build`

Genera la distrubución del proyecto utilizando webpack

## Depenencias

### `Webpack`

Se tienen las dependencias de webpack para poder empaquetar todas las dependencias y generar el servidor de desarrollo.

### `Axios`

Para comunicarnos con el servicio Rest.

### `Babel`

Es una herramienta que nos permite transformar nuestro código JS de última generación (o con funcionalidades extras) a JS que cualquier navegador o versión de node.

### `Boostrap`

Es de las principales bibliotecas para agregar componentes de Bootstrap a los proyectos de React.

### `Jest`

Como nuestro motor de pruebas.

### `Redux`

Es una librería JavaScript que ayudará en el el manejo del estado de la aplicación.